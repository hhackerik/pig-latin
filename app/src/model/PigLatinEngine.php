<?php

class PigLatinEngine
{
    CONST VOWELS = ["a","e","i","o","u","y"];

    /**
     * Translate function
     * @param $text
     * @return string
     */
    public function translate($text) {
        $words =  explode(" ", $text);
        $translatedTexts = [];
        foreach ($words as $key => $value) {
            $translatedTexts[] = strtolower($this->process($value));
        }

        return implode(" ",$translatedTexts);
    }

    /**
     * If first letter is vowel then we just add to end of word way
     * If two first letters are "qu" then letters we move to end and add ay end of word
     * The last option is that all the letters before any vowel letter. Then letters moves end of word and add ay
     * @param $word
     * @return string
     */
    private function process($word) {
        if ($this->isVowel(substr($word,0,1))) {
            return $word."-way ";
        }
        elseif(strtolower(substr($word,0,2)) === "qu") {
            return substr($word,2)."-quay";
        }
        else {
            $word = $this->parseConsonant($word);
            $word = $word."ay";
            return $word;
        }
    }

    /**
     * Parsing consonants
     * @param $word
     * @return string
     */
    private function parseConsonant($word)
    {
        $letters = str_split($word);
        $back = [];
        foreach ($letters as $key => $value) {
            if(!$this->isVowel($value)) {
                $back[] = $value;
                unset($letters[$key]);
            } else {
                break;
            }
        }

        return implode("",$letters)."-".implode("",$back);
    }

    /**
     * Check if word is Vowel
     * @param $letter
     * @return bool
     */
    private function isVowel($letter) {
        if (in_array(strtolower($letter),self::VOWELS)) return true;
        return false;
    }
}