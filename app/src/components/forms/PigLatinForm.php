<?php
use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

class PigLatinForm extends Control
{
	/**
	 * @return Form
	 */
	public function createComponentPigLatinForm()
	{
		$form = new Form;

        $form->addText('text', 'English text:')
			->setRequired("Zadejte prosím text!");

		$form->addSubmit("send", 'Translate');
		return $form;
	}


    /**
     * Form template
     */
    public function render()
	{
		$this->template->render(__DIR__ . '/templates/PigLatinForm.latte');
    }


}

interface IPigLatinForm
{
	/**
	 *
	 * @return PigLatinForm
	 */
	function create();
}