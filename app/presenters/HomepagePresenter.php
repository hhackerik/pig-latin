<?php

namespace App\Presenters;


use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;


class HomepagePresenter extends Presenter
{
    public $pigLatinForm;

    public $pigLatinEngine;

    public $translatedText = "";

    /**
     * HomepagePresenter constructor.
     * @param \IPigLatinForm $IPigLatinForm
     */
    public function __construct(\IPigLatinForm $IPigLatinForm, \PigLatinEngine $PigLatinEngine)
    {
        $this->pigLatinForm = $IPigLatinForm;
        $this->pigLatinEngine = $PigLatinEngine;
    }

    /**
     * Pig latin form
     * @return \PigLatinForm
     */
    public function createComponentPigLatinForm()
    {
        $form = $this->pigLatinForm->create();
        $form['pigLatinForm']->onSuccess[] = function (Form $form, $values) {
            $this->template->translatedText = $this->pigLatinEngine->translate($values->text);
        };
        return $form;
    }
}